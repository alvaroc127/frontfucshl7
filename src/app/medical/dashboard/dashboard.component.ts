import {Component} from '@angular/core';
import {modelmodule} from '../../model/model.module';
import {AuthservicesService} from '../../services/authservices.service';

@Component({
	selector:'dashboard',
	templateUrl:'./dashboard.component.html',
	styleUrls:['./dashboard.component.css']
})
export class  dashboard
{
	public sourcedash:number = 1;

	constructor(private auth:AuthservicesService)
	{

	}


	public getUsername():string
	{
	 try
     {
       	this.auth.getUserSession();
       	return this.auth.getUsername();
     }
     catch(e)
     {
       if(e == -1)
       {
         console.log('no login user',e);
       }
       sessionStorage.clear();
       return String(e);
     }
	}


	public getTitlesMedical():any
	{
		let navarTitlesMedical=
        [ 
            {title: 'Configuración Alertas', uri:'/configalert'} ,
            {title: 'Configuración de Usuario', uri:'/configusu'} ,
            {title: 'Consulta Personalizada', uri:'/consultper'},
            {title: 'Desvicular Usuario', uri:'/devincularusumo'} 
        ];
        return navarTitlesMedical;
	}


}