import { Component, OnInit } from '@angular/core';
import { Alertas}  from '../../../model/alertas.model';
import { Usuario}  from '../../../model/usuario.model';
import {NgForm} from '@angular/forms';
import {Rol} from '../../../model/rol.model';
import {Permiso} from '../../../model/permiso.model';
import {AuthservicesService} from '../../../services/authservices.service';
import {ConsultAlertSignalService} from '../../../services/consult-alert-signal.service';
import {MonitorconfigService} from '../../../services/monitorconfig.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-graficas',
  templateUrl: './graficas.component.html',
  styleUrls: ['./graficas.component.css']
})
export class GraficasComponent implements OnInit 
{

	public sourcedash:number = 1;

  public alertselect:Alertas=new Alertas();

  public alertasArr:Alertas[]=new Array();

  public indexalert:number=-1;

  public userselect:Usuario;

  public userbindMont:Usuario[];

  public indexarrsel:number=0;

  constructor(private rout:Router,private auth:AuthservicesService,
    private consulsig:ConsultAlertSignalService,
    private monticonf:MonitorconfigService) 
  {
    this.userbindMont=new Array();
    let per =new Permiso(1,'Permiso test');
    let permi=new Array();
    permi.push(per);
    let rol=new Rol({
      permiso:permi,
      id:1,
      descrtip:'Rol'
      });
    
    this.userselect= new Usuario({ 
         arraySingal:[],
         rol:rol,
         name:'',
         numerodoc:'',
         tipodoc:{id:'',descrip:''},
         mail:'',
         username:''
         });
  }

  public getUsername():string 
  {
  	 try
     {
       this.auth.getUserSession();
       return this.auth.getUsername();
     }catch(e)
     {
       if(e == -1)
       {
         console.log('no login user',e);
       }
       sessionStorage.clear();
       return String(e);
     }
  }


  /*public getAlertUser():Alertas[]
  {
    let alertas=new Array();


    let observat=
    {
      "obser_iden21":"[**FC demasiado bajo]",
      "obser_iden22":"[**SpO2 demasiado alto]",
      "obser_iden23":"[**FC demasiado bajo]",
      "obser_iden24":"[**SpO2 demasiado alto]",
      "ALARMAVF":"[0.060000]",
      "ALARMAVL":"[0.030000]",
      "ALARMAVR":"[-0.090000]",
      "ALARMCVP":"[0.000000]",
      "ALARMECGFR":"[60.000000]",
      "ALARMECGI":"[0.080000]",
      "ALARMECGII":"[0.100000]",
      "ALARMECGIII":"[0.020000]",
      "ALARMECGV":"[0.040000]",
      "ALARMECGECG1":"[125 125 125 126 126 128 129 129 130 130 131 132 132 132 131 130 130 129 129 127 127 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 124 123 122 121 121 126 133 137 145 152 159 162 155 148 140 133 126 120 121 122 123 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 126 127 127 129 129 130 131 132 132 133 134 135 135 135 136 136 137 137 137 138 138 138 137 137 136 136 135 135 135 133 133 133 131 130 129 129 127 126 126 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125]",
      "ALARMECGECG2":"[125 125 125 126 128 130 131 131 132 133 134 135 135 135 134 133 132 131 130 128 127 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 124 123 122 121 119 118 127 138 144 155 165 176 180 170 159 148 137 126 118 119 121 122 124 124 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 127 128 128 130 131 132 134 135 136 137 138 139 139 140 141 142 143 143 143 144 144 144 144 143 142 141 140 140 139 137 137 136 134 133 131 130 128 127 126 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125]",
      "ALARMECGECG3":"[128 128 128 128 129 129 129 130 130 130 130 130 131 130 130 130 130 130 129 129 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 127 127 128 128 128 127 127 128 129 130 131 132 134 134 134 130 125 121 117 113 112 116 120 125 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 129 128 128 129 128 129 129 129 129 128 129 129 129 129 129 129 129 129 129 129 129 129 130 129 129 129 130 130 130 130 130 130 130 130 131 130 130 130 130 129 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 129 128 128 128 128 128 128 128 128 128 128 129 128 128 128 128 128 128 127 127 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 127 127 128 128 128 129 128 128 128 128 129 128 128 128 127 128 128 128 128 128 128 128 128 128 128 128]",
      "patient_id":"101230671",
      "monitor":"192.168.1.10",
      "horasignal":"2021-04-01T15:36:05"
    };

let observat1=
    {
      "obser_iden21":"[**FC demasiado bajo]",
      "obser_iden22":"[**SpO2 demasiado alto]",
      "obser_iden23":"[**FC demasiado bajo]",
      "ALARMAVF":"[0.060000]",
      "ALARMAVL":"[0.030000]",
      "ALARMAVR":"[-0.090000]",
      "ALARMCVP":"[0.000000]",
      "ALARMECGFR":"[60.000000]",
      "ALARMECGI":"[0.080000]",
      "ALARMECGII":"[0.100000]",
      "ALARMECGIII":"[0.020000]",
      "ALARMECGV":"[0.040000]",
      "ALARMECGECG1":"[125 125 125 126 126 128 129 129 130 130 131 132 132 132 131 130 130 129 129 127 127 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 124 123 122 121 121 126 133 137 145 152 159 162 155 148 140 133 126 120 121 122 123 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 126 127 127 129 129 130 131 132 132 133 134 135 135 135 136 136 137 137 137 138 138 138 137 137 136 136 135 135 135 133 133 133 131 130 129 129 127 126 126 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125]",
      "ALARMECGECG2":"[125 125 125 126 128 130 131 131 132 133 134 135 135 135 134 133 132 131 130 128 127 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 124 123 122 121 119 118 127 138 144 155 165 176 180 170 159 148 137 126 118 119 121 122 124 124 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 127 128 128 130 131 132 134 135 136 137 138 139 139 140 141 142 143 143 143 144 144 144 144 143 142 141 140 140 139 137 137 136 134 133 131 130 128 127 126 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125]",
      "ALARMECGECG3":"[128 128 128 128 129 129 129 130 130 130 130 130 131 130 130 130 130 130 129 129 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 127 127 128 128 128 127 127 128 129 130 131 132 134 134 134 130 125 121 117 113 112 116 120 125 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 129 128 128 129 128 129 129 129 129 128 129 129 129 129 129 129 129 129 129 129 129 129 130 129 129 129 130 130 130 130 130 130 130 130 131 130 130 130 130 129 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 129 128 128 128 128 128 128 128 128 128 128 129 128 128 128 128 128 128 127 127 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 127 127 128 128 128 129 128 128 128 128 129 128 128 128 127 128 128 128 128 128 128 128 128 128 128 128]",
      "patient_id":"101230672",
      "monitor":"192.168.1.15",
      "horasignal":"2021-04-01T15:36:05"
    };
    let alerta1=new Alertas(observat);
    let alerta2=new Alertas(observat1);
    alertas.push(alerta1);
    alertas.push(alerta2);
    return alertas;
  }*/


  /*public setUserSelect()
  {
    console.log('seleciono el arreglo ', this.indexarrsel);
     this.alertselect.setobservation=
     this.getAlertUser()[this.indexarrsel].getobservation;
  }*/


  public setAlert()
  {
    console.log('seleciono el arreglo ', this.indexalert);
    this.alertselect=this.alertasArr[this.indexalert];
      if(!this.auth.isSesionOn())
    {
      alert('sesion off');
      this.auth.logoutUser();
      sessionStorage.clear();
      this.rout.navigate(['/login']);
      return;
    }
    try
    {
    this.auth.getUsername();
    }catch(e)
    {
      console.log('error en ',e);
      alert('sesion off');
      this.auth.logoutUser();
      sessionStorage.clear();
      this.rout.navigate(['/login']);
      return;
    }
  }

  public setUserSelect()
  {
    console.log('seleciono el arreglo ', this.indexarrsel);
    this.resetAlertSelect();
    this.alertasArr=new Array();
    this.indexalert=-1;
     this.userselect=this.userbindMont[this.indexarrsel];
      let fechaout=new Date();
     fechaout.setHours(fechaout.getHours()-5);
     let salid=fechaout.toJSON().replace('Z','');

     let usertoconsult=
     {
       username:this.userselect.username,
       dateTime:salid,//para la prueba
       ip:this.userselect.mail
     };
      if(!this.auth.isSesionOn())
    {
      alert('sesion off');
      this.auth.logoutUser();
      sessionStorage.clear();
      this.rout.navigate(['/login']);
      return;
    }
    try
    {
    this.auth.getUsername();
    }catch(e)
    {
      console.log('error en ',e);
      alert('sesion off');
      this.auth.logoutUser();
      sessionStorage.clear();
      this.rout.navigate(['/login']);
      return;
    }
    let token=this.auth.getBarerToken();
    this.consulsig.findAlertsbyPatient(usertoconsult,token).subscribe(
      response=>
      {
        if(response.status=='00')
        {
          let alertas=response.alerts;
          for(let ale in alertas)
          {
            let alert=new Alertas();
            alert.setdescripcion=alertas[ale].descripcion;
            alert.sethoraalert=alertas[ale].horaAlerta;
            alert.setseveridad=alertas[ale].severidad;
            this.alertasArr.push(alert);
          }

        }
      },error=>
      {
        this.alertasArr=new Array();
        this.indexalert=-1;
        this.resetAlertSelect();
        console.log('se genero el error en consulta ',error);
      }
      );


  }

  ngOnInit(): void 
  {
     if(!this.auth.isSesionOn())
    {
      alert('sesion off');
      this.auth.logoutUser();
      sessionStorage.clear();
      this.rout.navigate(['/login']);
      return;
    }
    try
    {
    this.auth.getUsername();
    }catch(e)
    {
      console.log('error en ',e);
      alert('sesion off');
      this.auth.logoutUser();
      sessionStorage.clear();
      this.rout.navigate(['/login']);
      return;
    }
    let token=this.auth.getBarerToken();
    this.monticonf.findUserMont(token).subscribe(
      response=>
      {
        if(response.status=='00')
        {
          let interol=
          {descrtip:'',id:10,permiso:new Array()};

          let usuarios=response.usuarios;
          for( let ind in usuarios)
          {
            let usser=usuarios[ind];
            let roles=usser.roles;
            for(let rol in roles)
            {
              let perm=new Permiso(1,'otro');
              let arrperm=new Array();
              arrperm.push(perm);
              interol=
                {
                descrtip:roles[rol].descrip,
                id:parseInt(rol,10),
                permiso:arrperm
                };
            }
            let role =new Rol(interol);
            let intus=
            { 
           arraySingal:new Array(),
           rol:role,
           name:'',
           numerodoc:usser.numerodoc,
           tipodoc:{id:usser.tipoDoc.idtipdoc,descrip:usser.tipoDoc.descrip},
           mail:usser.email,
           username:usser.tipoDoc.idtipdoc+usser.numerodoc
           };
           let userr=new Usuario(intus);
           this.userbindMont.push(userr);
         }
        }
      },error=>
        {
          console.log('no se pudo traer la consutla de usuarios vinculados',error);
        }
      );
  }

   public resetUserSelect()
  {
    this.userselect.username='';
    this.userselect.numerodoc='';
    this.userselect.rol=new Rol({
      descrtip:'',
      id:0,
      permiso:new Array()
    });
  }


  public resetAlertSelect()
  {
    this.alertselect.sethoraalert='';
    this.alertselect.setdescripcion='';
    this.alertselect.setseveridad='';
  }

}
