import { Component, OnInit,Input,OnChanges } from '@angular/core';
import {Alertas} from '../../../model/alertas.model';
import {ConsultAlertSignalService} from '../../../services/consult-alert-signal.service';
import {AuthservicesService} from '../../../services/authservices.service';
import {Router} from '@angular/router';
import { ChartDataset } from 'chart.js/auto';


@Component({
  selector: 'app-customconsult',
  templateUrl: './customconsult.component.html',
  styleUrls: ['./customconsult.component.css']
})
export class CustomconsultComponent implements OnInit,OnChanges {

 public sourcedash:number = 1;

  @Input() indexalert:number=0;

  @Input() horalerta:string='';
  @Input() username:string='';
  @Input() ip: string='';



  arrrgra:grafica[]=new Array();

  dates:string[]=new Array();



  constructor(private auth:AuthservicesService,
    private consser:ConsultAlertSignalService,
    private route:Router) 
  {  
    let i=1;
    while(i<7)
    {
      let graficaa=new grafica(i);
      this.arrrgra.push(graficaa);
      i++;
    }

  }

  ngOnInit(): void 
  {
    
   
  }

  ngOnChanges():void
  {
    if(this.indexalert !== -1)
    {
    this.setSelectAlert(this.indexalert);
    //this.OtherAlertSignal();
    }  

  }

/*public getAlertUser():Alertas[]
  {

    let alertas=new Array();
    // adjuntar sobre el observation de alert las alertas consultas
    let observat=
    {
      "obser_iden21":"[**FC demasiado bajo]",
      "obser_iden22":"[**SpO2 demasiado alto]",
      "obser_iden23":"[**FC demasiado bajo]",
      "obser_iden24":"[**SpO2 demasiado alto]",
      "ALARMAVF":"[0.060000]",
      "ALARMAVL":"[0.030000]",
      "ALARMAVR":"[-0.090000]",
      "ALARMCVP":"[0.000000]",
      "ALARMECGFR":"[60.000000]",
      "ALARMECGI":"[0.080000]",
      "ALARMECGII":"[0.100000]",
      "ALARMECGIII":"[0.020000]",
      "ALARMECGV":"[0.040000]",
      "ALARMECGECG1":"[125 125 125 126 126 128 129 129 130 130 131 132 132 132 131 130 130 129 129 127 127 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 124 123 122 121 121 126 133 137 145 152 159 162 155 148 140 133 126 120 121 122 123 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 126 127 127 129 129 130 131 132 132 133 134 135 135 135 136 136 137 137 137 138 138 138 137 137 136 136 135 135 135 133 133 133 131 130 129 129 127 126 126 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125]",
      "ALARMECGECG2":"[125 125 125 126 128 130 131 131 132 133 134 135 135 135 134 133 132 131 130 128 127 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 124 123 122 121 119 118 127 138 144 155 165 176 180 170 159 148 137 126 118 119 121 122 124 124 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 127 128 128 130 131 132 134 135 136 137 138 139 139 140 141 142 143 143 143 144 144 144 144 143 142 141 140 140 139 137 137 136 134 133 131 130 128 127 126 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125]",
      "ALARMECGECG3":"[128 128 128 128 129 129 129 130 130 130 130 130 131 130 130 130 130 130 129 129 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 127 127 128 128 128 127 127 128 129 130 131 132 134 134 134 130 125 121 117 113 112 116 120 125 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 129 128 128 129 128 129 129 129 129 128 129 129 129 129 129 129 129 129 129 129 129 129 130 129 129 129 130 130 130 130 130 130 130 130 131 130 130 130 130 129 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 129 128 128 128 128 128 128 128 128 128 128 129 128 128 128 128 128 128 127 127 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 127 127 128 128 128 129 128 128 128 128 129 128 128 128 127 128 128 128 128 128 128 128 128 128 128 128]",
      "patient_id":"101230671",
      "monitor":"192.168.1.10",
      "horasignal":"2021-04-01T15:36:05"
    };

    let observat1=
    {
      "obser_iden21":"[**FC demasiado bajo]",
      "obser_iden22":"[**SpO2 demasiado alto]",
      "obser_iden23":"[**FC demasiado bajo]",
      "ALARMAVF":"[0.060000]",
      "ALARMAVL":"[0.030000]",
      "ALARMAVR":"[-0.090000]",
      "ALARMCVP":"[0.000000]",
      "ALARMECGFR":"[60.000000]",
      "ALARMECGI":"[0.080000]",
      "ALARMECGII":"[0.100000]",
      "ALARMECGIII":"[0.020000]",
      "ALARMECGV":"[0.040000]",
      "ALARMECGECG1":"[150 150 150 126 126 128 129 129 130 130 131 132 132 132 131 130 130 129 129 127 127 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 124 123 122 121 121 126 133 137 145 152 159 162 155 148 140 133 126 120 121 122 123 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 126 127 127 129 129 130 131 132 132 133 134 135 135 135 136 136 137 137 137 138 138 138 137 137 136 136 135 135 135 133 133 133 131 130 129 129 127 126 126 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150 150]",
      "ALARMECGECG2":"[125 125 125 126 128 130 131 131 132 133 134 135 135 135 134 133 132 131 130 128 127 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 124 123 122 121 119 118 127 138 144 155 165 176 180 170 159 148 137 126 118 119 121 122 124 124 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 127 128 128 130 131 132 134 135 136 137 138 139 139 140 141 142 143 143 143 144 144 144 144 143 142 141 140 140 139 137 137 136 134 133 131 130 128 127 126 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125]",
      "ALARMECGECG3":"[128 128 128 128 129 129 129 130 130 130 130 130 131 130 130 130 130 130 129 129 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 127 127 128 128 128 127 127 128 129 130 131 132 134 134 134 130 125 121 117 113 112 116 120 125 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 129 128 128 129 128 129 129 129 129 128 129 129 129 129 129 129 129 129 129 129 129 129 130 129 129 129 130 130 130 130 130 130 130 130 131 130 130 130 130 129 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 129 128 128 128 128 128 128 128 128 128 128 129 128 128 128 128 128 128 127 127 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 128 127 127 128 128 128 129 128 128 128 128 129 128 128 128 127 128 128 128 128 128 128 128 128 128 128 128]",
      "patient_id":"101230672",
      "monitor":"192.168.1.15",
      "horasignal":"2021-04-01T15:36:05"
    };
    //let alerta1=new Alertas(observat);
    //let alerta2=new Alertas(observat1);
    let alerta1=new Alertas();
    let alerta2=new Alertas();
    alertas.push(alerta1);
    alertas.push(alerta2);
    return alertas;
  }*/



   public setSelectAlert(index:number)
   {
     console.log('SELECCINO UN ALERTA');
       if(!this.auth.isSesionOn())
    {
      alert('sesion off');
      this.auth.logoutUser();
      sessionStorage.clear();
      this.route.navigate(['/login']);
      return;
    }
    try
    {
    this.auth.getUsername();
    }catch(e)
    {
      console.log('error en ',e);
      alert('sesion off');
      this.auth.logoutUser();
      sessionStorage.clear();
      this.route.navigate(['/login']);
      return;
    }
    let token=this.auth.getBarerToken();
    let use=
    {
      username:this.username,
      ip:this.ip,
      dateTime:this.horalerta
    }
    this.consser.findSignalLastHour(use,token).subscribe(

      response=>
      {
        console.log('SELECCINO UN ALERTA',response);
       if(response.status=='00')
       {

         let alerttobs=response.observations;
         for(let obs in alerttobs)
         {
           let alertaa=new Alertas();
           alertaa.setobservation=alerttobs[obs];
           let observ=JSON.parse(alertaa.getobservation);
           let id=observ.id;
           let component=observ.component;
           if( component !== 'undefined')
           {
             let index=new Array;
             for( let comp in component )
             {
               let id2=component[comp].id;
               switch (id2)           
               {
               case "ECG1":
               console.log("es "+id2);
               index.push(component[comp]);
               console.log("es "+id2);
               break;

               case "ECG2":
               console.log("es "+id2);
               index.push(component[comp]);
               console.log("es "+id2);// code...
               break;

               case "ECG3":
               console.log("es "+id2);
               index.push(component[comp]);
               console.log("es "+id2);
               break;
             
               default : 
               //ECG1-[8 component]-desde cero es el 8
               //ECG2-[9 component]
               //ECG3-[10 compnent]
               console.log('estoy en default');
               break;
              }             
             } 
             if(index.length >0){
              this.loadECG(index);
              this.loadXdata();
            }
           }else
           {
             console.log('salida a produccion');
           }

          
          
         }
         
        }
      },
      error=>
        {
        console.log('error en la consulta de signal',error);
        }
      );
     //console.log("se ejecutar el select");
   }

  

   public convertStrArrNum(strnumber:string):number[]
   {
     //strnumber=strnumber.replace('[','');
     strnumber=strnumber.replace(/\s/g,',');   
     if(strnumber.charAt(strnumber.length - 1) ==',')
     {
       strnumber=strnumber.slice(0,-1);
     }
    if(strnumber.charAt(0)!='[')
     {
       strnumber='['+strnumber+']';
     }

     //console.log(strnumber);
     return JSON.parse(strnumber);
   }


   public loadECG(observ:any)
   {
     
     
       let dataecg1str=observ[0].valueSampledData.data;
         let ecgsig1=this.convertStrArrNum(dataecg1str);
         this.arrrgra[0].setdata=
             [
               {data: ecgsig1,
                label:'ecg1',
                borderColor:'#1AFA08',
                backgroundColor:'#1AFA08'
              }
             ];
     
       let dataecg2str=observ[1].valueSampledData.data;
         let ecgsig2=this.convertStrArrNum(dataecg2str);
         this.arrrgra[1].setdata=
             [
             {
             data:ecgsig2,
             label:'ecg2',
             borderColor:'#1AFA08',
             backgroundColor:'#1AFA08'
             }];
     
     
     
       let dataecg3str=observ[2].valueSampledData.data;
        let ecgsig3=this.convertStrArrNum(dataecg3str);
        this.arrrgra[2].setdata=
             [
             {
             data:ecgsig3,
             label:'ecg3',
             borderColor:'#1AFA08',
             backgroundColor:'#1AFA08'
             }];
                
   }

   public loadXdata()
   {

     let starDate=new Date(this.horalerta);
          let dataset=new Date(starDate);
          while(this.dates.length< this.arrrgra[0].getdata[0].data.length)
         {
         dataset=new Date(dataset);
         dataset.setMilliseconds(dataset.getMilliseconds()+1);
         this.dates.push(String(dataset.getMilliseconds()));
         }
   }
   

/* public OtherAlertSignal()
   {
     let arraySignal=
     [
    {
      "fullUrl": "http://localhost:9090/APIBackHl7-web/Observation/4",
      "resource": {
        "resourceType": "Observation",
        "id": "4",
        "valueSampledData": {
          "data": "98 98 98 98 98 98 98 98 98 98 98 98 98 98 98 99 99 99 99 100 100 100 100 100 101 101 102 102 102 103 103 103 104 104 104 105 105 105 106 106 106 107 107 108 108 109 109 109 110 110 111 111 112 112 112 113 113 114 114 115 115 116 116 117 117 118 118 118 119 119 120 120 121 121 121 123 123 123 123 124 125 125 126 126 126 127 127 128 129 129 129 131 131 131 132 133 133 133 134 134 135 136 137 137 138 139 139 140 140 141 142 143 143 143 145 145 145 146 147 147 147 149 149 150 150 151 151 152 "
        },
        "component": [
          {
            "id": "impedancia",
            "valueSampledData": {
              "data": "20.0"
            }
          },
          {
            "id": "idmon",
            "valueSampledData": {
              "data": "5"
            }
          }
        ]
      }
    },
    {
      "fullUrl": "http://localhost:9090/APIBackHl7-web/Observation/2",
      "resource": {
        "resourceType": "Observation",
        "id": "2",
        "valueSampledData": {
          "data": "3 2 2 2 2 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 2 3 4 5 7 9 11 13 15 17 20 22 24 27 30 32 35 37 39 42 44 46 49 51 53 56 58 60 62 64 66 68 70 73 74 76 78 80 82 83 84 86 87 88 88 89 90 90 90 90 90 89 89 89 89 88 87 86 85 84 83 82 81 79 78 76 74 73 71 69 68 66 65 63 61 60 58 56 55 53 51 50 49 48 47 45 44 43 41 40 39 38 36 35 34 33 32 32 31 31 31 31 30 30 30 30 29 29 29 29 29 29 30 30 30 31 31 32 32 33 33 33 33 34 34 34 34 34 34 34 34 34 33 33 33 33 32 32 32 32 31 31 30 30 29 29 29 28 28 27 27 27 27 26 26 25 25 24 24 24 24 24 23 23 22 22 21 21 20 20 19 19 18 18 17 17 17 17 17 16 16 16 16 15 15 15 15 14 14 13 13 12 12 12 12 12 11 11 10 10 9 9 9 9 8 8 7 7 6 6 6 6 6 5 5 5 5 4 4 4 4 4 4 4 4 "
        },
        "component": [
          {
            "id": "desconocido",
            "valueSampledData": {
              "data": "98.0"
            }
          },
          {
            "id": "frecuencia",
            "valueSampledData": {
              "data": "60.0"
            }
          },
          {
            "id": "idmon",
            "valueSampledData": {
              "data": "5"
            }
          }
        ]
      }
    },
    {
      "fullUrl": "http://localhost:9090/APIBackHl7-web/Observation/3",
      "resource": {
        "resourceType": "Observation",
        "id": "3",
        "valueSampledData": {
          "data": "0 9 0 9 0 9 0 9 0 9 0 9 0 9 0 9 0 9 0 9 0 9 0 9 0 9 0 9 0 9 0 9 0 9 0 9 0 9 0 9 0 9 0 9 0 9 0 9 0 9 0 9 0 9 0 9 0 9 0 9 0 9 0 9 0 10 0 12 0 14 0 15 0 17 0 18 0 20 0 20 0 21 0 22 0 23 0 24 0 25 0 25 0 25 0 26 0 26 0 26 0 26 0 25 0 25 0 24 0 24 0 23 0 22 0 22 0 20 0 20 0 19 0 19 0 18 0 18 0 18 0 18 0 18 0 18 0 18 0 18 0 18 0 18 0 18 0 18 0 18 0 17 0 17 0 17 0 16 0 16 0 15 0 15 0 15 0 14 0 14 0 13 0 13 0 12 0 12 0 12 0 12 0 11 0 11 0 11 0 11 0 11 0 11 0 10 0 10 0 10 0 10 0 10 0 10 0 10 0 9 0 9 0 9 0 9 0 9 0 9 0 9 0 9 0 9 0 9 0 9 0 9 0 9 0 9 0 9 0 9 0 9 0 9 0 9 0 9 0 9 0 9 0 9 0 9 "
        },
        "component": [
          {
            "id": "Maximo",
            "valueSampledData": {
              "data": "25.0"
            }
          },
          {
            "id": "minimo",
            "valueSampledData": {
              "data": "14.0"
            }
          },
          {
            "id": "parentesis",
            "valueSampledData": {
              "data": "9.0"
            }
          },
          {
            "id": "idmon",
            "valueSampledData": {
              "data": "5"
            }
          }
        ]
      }
    },
    {
      "fullUrl": "http://localhost:9090/APIBackHl7-web/Observation/3",
      "resource": {
        "resourceType": "Observation",
        "id": "3",
        "valueSampledData": {
          "data": "0 77 0 76 0 76 0 76 0 76 0 76 0 76 0 76 0 76 0 76 0 76 0 76 0 76 0 76 0 76 0 76 0 76 0 76 0 76 0 76 0 76 0 76 0 76 0 76 0 76 0 76 0 76 0 76 0 76 0 76 0 76 0 76 0 76 0 76 0 76 0 76 0 76 0 76 0 76 0 76 0 76 0 76 0 76 0 78 0 80 0 84 0 89 0 94 0 99 0 103 0 106 0 109 0 112 0 114 0 115 0 117 0 118 0 119 0 120 0 120 0 121 0 121 0 121 0 120 0 120 0 119 0 118 0 116 0 115 0 113 0 111 0 110 0 108 0 107 0 106 0 105 0 105 0 105 0 105 0 106 0 107 0 107 0 108 0 108 0 107 0 107 0 106 0 105 0 104 0 103 0 101 0 99 0 97 0 96 0 94 0 92 0 91 0 89 0 88 0 87 0 86 0 85 0 84 0 84 0 83 0 83 0 82 0 82 0 82 0 82 0 81 0 80 0 80 0 80 0 80 0 80 0 80 0 79 0 79 0 79 0 79 0 78 0 78 0 78 0 78 0 78 0 77 0 77 "
        },
        "component": [
          {
            "id": "Maximo",
            "valueSampledData": {
              "data": "120.0"
            }
          },
          {
            "id": "minimo",
            "valueSampledData": {
              "data": "90.0"
            }
          },
          {
            "id": "parentesis",
            "valueSampledData": {
              "data": "75.0"
            }
          },
          {
            "id": "idmon",
            "valueSampledData": {
              "data": "5"
            }
          }
        ]
      }
    }
     ];
     let i=0;
     let sigcon=0;
     while(i< arraySignal.length)
     {
      let tip=arraySignal[i].resource;
      let strdata=tip.valueSampledData.data;
      let numberdata=this.convertStrArrNum(strdata);
      switch (tip.id) 
      {
          case '4':
          this.arrrgra[5].setdata=
          [
           {data:numberdata, 
             label:'FrecResp',
             backgroundColor:'#006ffd',
             borderColor:'#006ffd',
           }
          ];
          break;

          case '2':
          this.arrrgra[2].setdata=
          [ 
          {
            data:numberdata,
            label:'SPO2',
           borderColor:'#00FDDE',
           backgroundColor:'#00FDDE'
          }
          ];
          break;

          case '3':
          if(sigcon == 0)
          {
            this.arrrgra[6].setdata=
            [ {data:numberdata,
               label:'Yellow',
               borderColor:'#EEFD00',
               backgroundColor:'#EEFD00'
             }
            ];
            sigcon++;
          }
          if(sigcon >= 1)
          {
            this.arrrgra[7].setdata=
            [{
              data:numberdata,
               label:'Red',
               borderColor:'#FD0000',
               backgroundColor:'#FD0000'
              }
            ];
          }
          break;
        
        default:

          break;
      }

     }
  }*/

}


export class grafica 
{

  typechar:any ='line';
  tipomedical:number;
  data:ChartDataset[]=[];
  label:string;
  option:any;
  color:string;
 




// los tipos que pide el cnavas, son los atributos.
  constructor(tip:number)
  {
    this.tipomedical=tip;
   
    switch (this.tipomedical)
     {
      case 1:
      this.label='ecg1';
      this.color='#1AFA08';
      
        // code...
      break;

      case 2:
      this.label='ecg2';
      this.color='#1AFA08';

        // code...
      break;


      case 3:
      this.label='ecg3';
      this.color='#1AFA08';

        // code...
       break;


       case 4:
      this.label='spo2';
      this.color='#00E9FC';
        // code...
       break;

       case 5:
       this.label='FredResp';
       this.color='#0058FC';
        // code...
       break;


       case 6:
       this.label='Yellow';
       this.color='#FDF900';
        // code...
       break;


       case 7:
       this.label='Red';
       this.color='#FD0000';
        // code...
       break;
      
      default:
        this.label='Grafica';
        this.color='#1AFA08';
        break;
    }
     this.option=
    {
      reponsive:true,
      color:this.color
     
    }

  }


  get getdata():ChartDataset[]
  {
    return this.data;
  }

  set setdata(data1:ChartDataset[])
  {
    this.data=data1;
  }

  get gettypecharr():any
  {
    return this.typechar;
  }

  set settypechar(typch:any)
  {
    this.typechar=typch;
  }

}
