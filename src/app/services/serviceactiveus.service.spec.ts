import { TestBed } from '@angular/core/testing';

import { ServiceactiveusService } from './serviceactiveus.service';

describe('ServiceactiveusService', () => {
  let service: ServiceactiveusService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServiceactiveusService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
