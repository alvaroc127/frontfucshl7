import { Injectable } from '@angular/core';
import {PractionerDAO} from '../model/repositority/datasource/model.repository.datasource.PractioneDAO';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AlertconfigService 
{

 

  constructor(private practidao:PractionerDAO) 
  { 
  }


  public activeMovil(user:any,token:string):Observable<any>
  {
  	return this.practidao.activeMovil(user,token);
  }

  public activeAlert(user:any,token:string):Observable<any>
  {
  	return this.practidao.activeAlert(user,token);
  }

   public unactiveMovil(user:any,token:string):Observable<any>
  {
  	return this.practidao.unactveMovil(user,token);
  }

  public unactiveAlert(user:any,token:string):Observable<any>
  {
  	return this.practidao.unactiveAlert(user,token);

  }






}
