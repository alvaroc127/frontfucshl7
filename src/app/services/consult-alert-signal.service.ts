import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {SignalDAO} from '../model/repositority/datasource/model.repository.datasource.SignalDAO';

@Injectable({
  providedIn: 'root'
})
export class ConsultAlertSignalService 
{
  constructor(private sign:SignalDAO) 
  {

  }


  public  findAlertsbyPatient(user:any, token:string):Observable<any>
  {
  	return this.sign.findAlertsbyPatient(user,token);
  }

  public findSignalLastHour(user: any, token: string):Observable<any>
  {
  	return this.sign.findSignalLastHour(user,token);

  }




}
