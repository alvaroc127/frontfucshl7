import { TestBed } from '@angular/core/testing';

import { UnactivateuserService } from './unactivateuser.service';

describe('UnactivateuserService', () => {
  let service: UnactivateuserService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UnactivateuserService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
