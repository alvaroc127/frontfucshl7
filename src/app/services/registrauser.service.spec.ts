import { TestBed } from '@angular/core/testing';

import { RegistrauserService } from './registrauser.service';

describe('RegistrauserService', () => {
  let service: RegistrauserService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RegistrauserService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
