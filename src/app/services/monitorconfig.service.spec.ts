import { TestBed } from '@angular/core/testing';

import { MonitorconfigService } from './monitorconfig.service';

describe('MonitorconfigService', () => {
  let service: MonitorconfigService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MonitorconfigService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
