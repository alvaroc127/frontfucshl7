import { Injectable } from '@angular/core';
import {DeviceDetectorService } from 'ngx-device-detector';

@Injectable({
  providedIn: 'root'
})
export class DispositivService 
{
	public deviceInfo:any;
    isDesktopDevice: boolean=true;
 	isTablet: boolean=false;
 	isMobile: boolean=false;

  constructor(private devicedect:DeviceDetectorService) 
  { 
  }


  public getInfoDevice()
  {
  	this.deviceInfo=this.devicedect.getDeviceInfo();
  	this.isDesktopDevice=this.devicedect.isDesktop();
  	this.isMobile=this.devicedect.isMobile();
  	this.isTablet=this.devicedect.isTablet();
  }

  public getDeviceType():string 
  {
  	return this.deviceInfo.deviceType;
  }

  public getDeviceOS():string
  {
  	return this.deviceInfo.os;
  }



}
