import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import {loginModule} from './login/login.module';
import {HttpClientModule} from '@angular/common/http';
import { AppComponent } from './app.component';
import {dashboarModule} from './medical/dashboard/dashboard.module';
import { AdminDashBoardModule } from './admin/dashboard/admin.dashboard.module';
import {navBarModule} from './navbar/dashboard.navbar.module'
import { ChartsModule } from 'ng2-charts';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule, 
    ReactiveFormsModule,
    loginModule,
    AdminDashBoardModule,
    dashboarModule,
    navBarModule,
    ChartsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
