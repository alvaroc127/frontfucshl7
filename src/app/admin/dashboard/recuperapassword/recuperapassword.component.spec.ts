import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecuperapasswordComponent } from './recuperapassword.component';

describe('RecuperapasswordComponent', () => {
  let component: RecuperapasswordComponent;
  let fixture: ComponentFixture<RecuperapasswordComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecuperapasswordComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecuperapasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
