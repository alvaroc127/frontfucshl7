import { Component, OnInit } from '@angular/core';
import {modelmodule} from '../../model/model.module';
import {AuthservicesService} from '../../services/authservices.service';

@Component({
  selector: 'app-admin.dashboard',
  templateUrl: './admin.dashboard.component.html',
  styleUrls: ['./admin.dashboard.component.css']
})

export class AdminDashboardComponent implements OnInit 
{
	public sourchedash:number =2;
  ngOnInit(): void {
  }



  constructor(private auth1:AuthservicesService)
  {


  }



	public getUsername():string
	{
		 try
     {
       this.auth1.getUserSession();
       return this.auth1.getUsername();
     }catch(e)
     {
       if(e == -1)
       {
         console.log('no login user',e);
       }
       sessionStorage.clear();
       return String(e);
     }


	}


  public getComponents():any
  {
      let navarTitlesAdmin=
     [
         {title: 'Registrar Usuario', uri:'/registrauser'},
         {title: 'Cambiar Password', uri:'/recoverpass'},
         {title: 'Activar Usuario', uri:'/adcionarrol'},
         {title: 'Desactivar Usuario', uri:'/quitarrol'}
     ] ;
     return navarTitlesAdmin;

  }


  
}
