import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {Router} from '@angular/router';
import {Rol} from '../../../model/rol.model';
import {Permiso} from '../../../model/permiso.model';
import { AuthservicesService} from '../../../services/authservices.service';
import {ServiceactiveusService} from '../../../services/serviceactiveus.service';


@Component({
  selector: 'app-adicionarrolcomponent',
  templateUrl: './adicionarrolcomponent.component.html',
  styleUrls: ['./adicionarrolcomponent.component.css']
})
export class AdicionarrolcomponentComponent implements OnInit 
{
  public sourchedash:number= 2;
  public roles:Rol[] = new Array();


  constructor(private auth1:AuthservicesService,
    private rout:Router,private serviceaact:ServiceactiveusService) 
  {

  }


  public getUsername():string 
  {

     try
     {
       this.auth1.getUserSession();
       return this.auth1.getUsername();
     }catch(e)
     {
       if(e == -1)
       {
         console.log('no login user',e);
         this.rout.navigate(['/login']);
       }else
       {
         this.rout.navigate(['/login']);
         this.auth1.logoutUser();
         sessionStorage.clear();
       }
       return e;
     }
  }

  ngOnInit(): void 
  {
    this.getRoles();
  }


  public getTypeDoc():any
  {
  	let array=
  	[
  		{ id: '1', descrip:'cédula' },
  		{ id: '2', descrip:'cédula extranjería' },
  		{ id: '3', descrip:'tarjeta identidad' }
  	]
  	return array;
  }


  public getRoles()
  {
    let rolesarr=new Array();
    if(!this.auth1.isSesionOn())
    {
      alert('sesion off');
      this.auth1.logoutUser();
      sessionStorage.clear();
      this.rout.navigate(['/login']);
      return;
    }

    try
    {
      this.auth1.getUsername();
    }catch(e)
    {
      console.log('error en ',e);
      alert('sesion off');
      this.auth1.logoutUser();
      sessionStorage.clear();
      this.rout.navigate(['/login']);
      return;
    }
    let token=this.auth1.getBarerToken();
    this.serviceaact.consultRols(token).subscribe(
      response=>
      {
        if(response.status=='00')
        {
          let rols=response.map;
          for(const rolpro in rols)
          {
            let permiso=rols[rolpro].permisos;
            let idrol=parseInt(rolpro,10);
            let descrol=rols[rolpro].descrip;
            let arrpermiso=new Array();
            for(const perpro in permiso)
            {
              let id=parseInt(permiso[perpro].id,10);
              let descrip=permiso[perpro].descrip;
              let perm=new Permiso(id,descrip);
              arrpermiso.push(perm);
            }            
            let rol=new Rol(
            {
              permiso: arrpermiso,
              id: idrol,
              descrtip: descrol
            });
            rolesarr.push(rol);
          }
          this.roles=rolesarr;
          this.roles=this.roles.filter(
           function(rol:Rol)
           {
             return rol.id !== 4;
          });
        } 

      },
      error=>
      {
        console.log('algo paso un error ',error);
      }
      );
  }

  useractive=
  {
    username:'',
    statusUser:'',
    roles:[
    {
      id:'',
      estado:'',
      descrip:'',
      permisos:{}
    }]
  }

  userform=
  {
    tipodoc:'',
    numerodoc:'',
    idrol:''
  }


  public activeUserRol(useractiv:any)
  {
    if(!this.auth1.isSesionOn())
    {
      alert('sesion off');
      this.auth1.logoutUser();
      sessionStorage.clear();
      this.rout.navigate(['/login']);
      return;
     }

    try
    {
      this.auth1.getUsername();
    }catch(e)
    {
      console.log('error en ',e);
      alert('sesion off');
      this.auth1.logoutUser();
      sessionStorage.clear();
      this.rout.navigate(['/login']);
      return;
    }

    this.useractive.username=
    useractiv.tipodoc+useractiv.numerodoc;
    this.useractive.statusUser='1';
    let rolUser=this.roles.find( (rol:Rol)=>
    {
     return  rol.id==useractiv.idrol;
    });
    if(rolUser !==undefined)
    {
      this.useractive.roles[0]=
      {
        id:String(rolUser.id),
        estado:'1',
        descrip:rolUser.descrtip,
        permisos:{}
       };
       console.log('se envia el objeto ',this.useractive);
       console.log('se envia el objeto ',this.useractive.roles);
       let token=this.auth1.getBarerToken();
    this.serviceaact.activAsigRol(this.useractive,token).
    subscribe(
      response=>
      {
        if(response.status=='00')
        {
          alert('se activo el usuario de forma correcta');
        }

      },
      error=>
      {
        alert('no se pudo activar el usuario');
        console.log('ocurrio un error activando el usuario',error);
      }
      );
    }else
    {
      console.log("no se pudo encontrar el rol");
    }
    
    this.restObj();
  }

  public restObj()
  {
    this.userform.tipodoc='';
    this.userform.numerodoc='';
    this.userform.idrol='';

    this.useractive.username='';
    this.useractive.statusUser='';
    this.useractive.roles=new Array();
  }

}
