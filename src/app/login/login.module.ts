import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import {login} from './login.component.login';
import {modelmodule} from '../model/model.module';
import { RouterModule, Routes } from '@angular/router';
import {AdminDashBoardModule} from '../admin/dashboard/admin.dashboard.module';
import {dashboarModule} from '../medical/dashboard/dashboard.module';
import {navBarModule} from '../navbar/dashboard.navbar.module';




const routes:Routes =
[
	{path:"login", component:login},
	{path:"admindashboard", loadChildren:'./admin/dashboard/admin.dashboard.module#AdminDashBoardModule'
	},
	{path:"medicaldashboard", loadChildren:'./medical/dashboard/dashboard.module#dashboarModule'
	}
];

@NgModule({
	imports: [modelmodule,BrowserModule, 
    ReactiveFormsModule,
	FormsModule,
	AdminDashBoardModule,
	dashboarModule,
	navBarModule,
	RouterModule.forRoot(routes)],
	declarations:[login],
	exports:[RouterModule,login],
	providers:[]
})

export class loginModule{}











