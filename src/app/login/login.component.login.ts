import {Component,OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import {modelmodule} from '../model/model.module';
import {AuthservicesService} from '../services/authservices.service';




@Component({
	selector: 'login',
	templateUrl: './login.Component.html',
	styleUrls: ['login.Component.css']
})




export class login implements OnInit
{

	public error:string='';
	public ngOnInit()
	{
		this.auth.isStartSesion().subscribe
		(
			subsribe=>
			{
				try
				{
					if(subsribe == 'failsesion')
					{
						this.error='ocurrio un error iniciando sesion';
						alert(this.error);
					}
					else
					{
				 		this.auth.getUserSession();
				 		if(this.auth.isAdmin())
				 		{
				 			console.log('es un medico');
				 			this.route.navigate(['/admindashboard']);
				 		}
				 		else
				 	    {
				 	    	//console.log('es un medico'+ this.auth.isDoctorNurse());
				 		if(this.auth.isDoctorNurse())
				 		{
				 			console.log('es un medico');
				 			this.route.navigate(['medicaldashboard']);
				 		}
				 	  }
				 		
					}
				}catch(e)
				{
					console.log("ocurrio un error");
					console.log(e);
				}
			},
			error=>
			{
				console.log("ocurrio un error en la susbcripcion");

			}
		)

	}


	formuser=
	{
		numerodoc:'',
		tipodoc:'',
		username:'',
		password:''
	}

	


	resetvar()
	{
		this.formuser.numerodoc='';
		this.formuser.tipodoc='';
		this.formuser.password='';
		this.formuser.username='';
	}


	constructor(private auth:AuthservicesService,private route:Router)
	{


	}


	public getTipoDocumento():any 
	{

		 let array=
    [
      { id: '1', descrip:'cédula' },
      { id: '2', descrip:'cédula extranjería' },
      { id: '3', descrip:'tarjeta identidad' }
    ]
    	return array;
	}

	public onsubimitform(form:any)
	{
		form.username= form.tipodoc+form.numerodoc;
		if(form.tipodoc !=='' && form.numerodoc !== '' && form.password !== '')
		{
		this.auth.authUser(form.username,form.password);
		this.resetvar();
		}
	}

}