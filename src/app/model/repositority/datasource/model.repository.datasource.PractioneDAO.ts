import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import {environment}from '../../../../environments/environment';

@Injectable()
export class PractionerDAO
{

	private url:string;
 	constructor(private httpcli:HttpClient)
 	{
 		this.url='';
 	}


 	public activeMovil(user:any,token:string):Observable<any>
 	{
 		this.url=environment.baseurl+environment.medicalendpoints.activemov;
 		let httphead=new HttpHeaders
		({
			'Authorization':`Bearer ${token}`,
			'UUID':'123456',
			'Content-Type':'application/json',
			'source':'ANG-S001'
		});
		return this.httpcli.post<any>(this.url,user,{'headers':httphead});
 	}


 	public activeAlert(user:any,token:string):Observable<any>
 	{
 		this.url=environment.baseurl+environment.medicalendpoints.activealert;
 		let httphead=new HttpHeaders
		({
			'Authorization':`Bearer ${token}`,
			'UUID':'123456',
			'Content-Type':'application/json',
			'source':'ANG-S001'
		});
		return this.httpcli.post<any>(this.url,user,{'headers':httphead});
 	}

 	public unactveMovil(user:any,token:string):Observable<any>
 	{
 		this.url=environment.baseurl+environment.medicalendpoints.unactivemov;
 		let httphead=new HttpHeaders
		({
			'Authorization':`Bearer ${token}`,
			'UUID':'123456',
			'Content-Type':'application/json',
			'source':'ANG-S001'
		});
		return this.httpcli.post<any>(this.url,user,{'headers':httphead});
 	}

 	public unactiveAlert(user:any,token:string):Observable<any>
 	{
 		this.url=environment.baseurl+environment.medicalendpoints.unactivealer;
 		let httphead=new HttpHeaders
		({
			'Authorization':`Bearer ${token}`,
			'UUID':'123456',
			'Content-Type':'application/json',
			'source':'ANG-S001'
		});
		return this.httpcli.post<any>(this.url,user,{'headers':httphead});
 	}


 	



}

