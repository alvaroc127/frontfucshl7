import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
@Injectable()
export class UserDAO
{
	protected url:string ='';

     constructor(private httpcl: HttpClient)
     {
     	
     }

	public activeRolUser(useractive:any,tokenHead:string):Observable<any>
	{
		this.url=environment.baseurl+environment.adminsendpoints.activeuser;
		let httphead=new HttpHeaders
		({
			'Authorization':`Bearer ${tokenHead}`,
			'UUID':'123456',
			'Content-Type':'application/json',
			'source':'ANG-S001'
		});	
		return this.httpcl.post<any>(this.url,useractive,{'headers':httphead});
	}


	public registrarUsuario(usuario:any,tokenHead:string):Observable<any>
	{
		this.url=environment.baseurl+environment.adminsendpoints.registrausuario;
		let httphead=new HttpHeaders
		({
			'Authorization':`Bearer ${tokenHead}`,
			'UUID':'123456',
			'Content-Type':'application/json',
			'source':'ANG-S001'
		});	
		return this.httpcl.post<any>(this.url,usuario,{'headers':httphead});

	}


	public changePassword(usuario:any,tokenHead:string):Observable<any>
	{
		this.url=environment.baseurl+environment.adminsendpoints.changepass;
		let httphead=new HttpHeaders
		({
			'Authorization':`Bearer ${tokenHead}`,
			'UUID':'123456',
			'Content-Type':'application/json',
			'source':'ANG-S001'
		});
		return this.httpcl.post<any>(this.url,usuario,{'headers':httphead});

	}


	public consultRols(tokenHead:string):Observable<any>
	{
		this.url=environment.baseurl+environment.adminsendpoints.roles;
		let httphead=new HttpHeaders
		({
			'Authorization':`Bearer ${tokenHead}`,
			'UUID':'123456',
			'Content-Type':'application/json',
			'source':'ANG-S001'
		});
		return this.httpcl.post<any>(this.url,{},{'headers':httphead});
	}


	public unactiveRolUser(userunactive:any,tokenHead:string):Observable<any>
	{
		this.url=environment.baseurl+environment.adminsendpoints.unactive;
		let httphead=new HttpHeaders
		({
			'Authorization':`Bearer ${tokenHead}`,
			'UUID':'123456',
			'Content-Type':'application/json',
			'source':'ANG-S001'
		});
		return this.httpcl.post<any>(this.url,userunactive,{'headers':httphead});
	}

}